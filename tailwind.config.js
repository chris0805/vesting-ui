module.exports = {
    mode: "jit",
    purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            fontWeight: {
                regular: 400,
            },
        },
        fontFamily: {
            museosans: "MuseoSans",
        },
    },
    variants: {
        extend: {},
    },
    plugins: [require("tailwindcss"), require("precss"), require("autoprefixer")],
};
