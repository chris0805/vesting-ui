import Web3 from "web3";

declare global {
  interface Window {
    ethereum: any;
    web3: any;
  }
}

const getWeb3 = async () =>
  new Promise(async (resolve, reject) => {
    try {
      if (window.ethereum) {
        const web3 = new Web3(window.ethereum);
        await window.ethereum.request({ method: 'eth_requestAccounts' });
        resolve(web3);
      } else if (window.web3) {
        const web3 = window.web3;
        console.log("Injected web3 detected.");
        resolve(web3);
      } else {
        resolve(null);
      }
    } catch (error) {
      reject(null);
    }
  });
export default getWeb3;
