import React, { useState } from "react";
import styles from "./shill-allotment.module.scss";
import CustomButton from "./shared/custom-button";
import { Contracts } from "../contracts/contact-list";

const ShillAllotment = (props) => {
    console.log("props", props);
    const [amountToClaim, setAmountToClaim] = useState("0");

    const onAmountToClaimChange = (e) => {
        console.log('claimvalue', e);
        const newAmountToClaim = e.target.value;
        setAmountToClaim(newAmountToClaim);
    };
    console.log(props);
    return (
        <div className={styles["shill-allotment-container"]}>
            <section className={styles["shill-allotment-current"]}>
                <p style={{width: '100%', paddingLeft: '8px'}}>
                    <select className={styles["shill-allotment-select"]} name="contract" id="contracts">
                        {Contracts.map(contract => (
                            <option key={contract.name} value={contract.name}>{contract.name}</option>
                        ))}
                    </select>
                </p>
            </section>
            <section className={styles["shill-allotment-current"]}>
                <h3>SHILL Allotment</h3>
                <p>
                    <img className={styles["SHILL-logo"]} alt="SHILL-logo" src="/SHILL_Logo.svg" />
                    {props.shillAllotment}
                    <span className={styles["i-description-container"]} data-tool-tip="some SHILL Allotment information">
                        <img className={styles["i-description"]} alt="i-description" src="/i-description.svg" />
                    </span>
                </p>
            </section>

            <section className={styles["shill-allotment-claimable"]}>
                <div className={styles["shill-allotment-claim-input"]}>
                    <h3>Claimable</h3>
                    <input type="number" placeholder="0" name="claim-amount" value={props.amountToClaim}  disabled />
                    <img className={styles["SHILL-logo"]} alt="SHILL-logo" src="/SHILL_Logo.svg" />
                    <span className={styles["i-description-container"]} data-tool-tip="some Claimable information">
                        <img className={styles["i-description"]} alt="i-description" src="/i-description.svg" />
                    </span>
                    <span className={styles["shill-allotment-claim-date"]}>
                        per &nbsp;<span>{props.end ?props.end: null}</span>
                    </span>
                </div>
                {props.currentStep === props.step?.STEP_4 ? (
                    <CustomButton backgroundColor="#c4c4c4" onClick={props.onClaimClick.bind(this, props.amountToClaim)} disabled>
                        Claimed
                    </CustomButton>
                ) : (
                    <CustomButton backgroundColor={props.amountToClaim? "#6bb9a0": "#c4c4c4"} onClick={props.onClaimClick.bind(this, props.amountToClaim)} disabled={!props.amountToClaim}>
                        Claim
                    </CustomButton>
                )}
            </section>

            <section className={styles["shill-allotment-claimed-token"]}>
                <h3>Claimed Token</h3>
                <p>
                    <img className={styles["SHILL-logo"]} alt="SHILL-logo" src="/SHILL_Logo.svg" />
                    {props.claimedToken || 0}
                    <span className={styles["i-description-container"]} data-tool-tip="some Claimed Token information">
                        <img className={styles["i-description"]} alt="i-description" src="/i-description.svg" />
                    </span>
                </p>
            </section>

            <section className={styles["shill-allotment-unlock-progress"]}>
                <div className={styles["shill-allotment-unlock-progress-title"]}>
                    <p>Unlock Progress</p>
                    <span className={styles["i-description-container"]} data-tool-tip="some Unlock Progress information">
                        {props.progress}% <img className={styles["i-description"]} alt="i-description" src="/i-description.svg" />
                    </span>
                </div>
                <div className={styles["shill-allotment-progress-bar-border"]}>
                    <div className={styles["shill-allotment-progress-bar"]} style={{ width: props.progress+"%" }}>
                        <img className={styles["SHILL-logo"]} alt="SHILL-logo" src="/SHILL_Logo.svg" />
                    </div>
                </div>
            </section>
        </div>
    );
};

export default ShillAllotment;
