import React from "react";
import styles from "./claim-box.module.scss";
import CustomButton from "./shared/custom-button";

const ClaimBox = (props) => {
    return (
        <div className={styles["claim-box-container"]}>
            <h3>You&apos;re about to claim your Seed Round.</h3>
            <p className={styles["claim-amount"]}>
                <img alt="SHILL Logo" src="/SHILL_Logo.svg" />
                {props.amountToClaim}
            </p>
            <p className={styles["confirmation-message"]}>Are you sure you want to claim your Seed Round?</p>
            <div className={styles["confirmation-button"]}>
                <CustomButton backgroundColor="#6bb9a0" onClick={props.onConfirmationClick.bind(this, props.amountToClaim, props.vestingAddr)}>
                    Claim
                </CustomButton>
                <CustomButton onClick={props.onCancelClick}>Back</CustomButton>
            </div>
        </div>
    );
};

export default ClaimBox;
