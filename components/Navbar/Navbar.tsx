import React, { Fragment, useState } from "react";
import Link from "next/link";
import styles from "./Navbar.module.scss";
import CustomButton from "../shared/custom-button";
import { useAppContext } from "../../context/state";
import { STEP } from "../../constants";
import { useRouter } from "next/dist/client/router";

const Navbar = (props) => {
    
    const { currentStep, setCurrentStep, setWeb3Account } = useAppContext();
    const router = useRouter();

    const handleDisconnectWallet = () => {
        setWeb3Account(null); //Clear current wallet
        setCurrentStep(STEP.STEP_1); //Go back home
        delete router.query['wallet'];
        router.push(router);
    }

    return (
        <Fragment>
            <nav className={styles["navbar-container"]}>
                <div className={styles["navbar-sub-container"]}>
                    <Link href="/" passHref>
                        <img alt="Project Seed Logo" src="/PSLogo.svg" />
                    </Link>

                    {currentStep !== STEP.STEP_1 && (
                        <CustomButton backgroundColor="#6bb9a0" maxWidth="150px" onClick={handleDisconnectWallet}>
                            Disconnect Wallet
                        </CustomButton>     
                    )}
                    </div>
            </nav>
            <Fragment>{props.children}</Fragment>
        </Fragment>
    );
};

export default Navbar;
