import React from 'react';
import BalanceInformations from './balance-informations';
import ConnectWallet from './connect-wallet';
import ClaimBox from './claim-box';
import ShillAllotment from './shill-allotment';
import styles from './invest-dashboard.module.scss';
import { useAlert } from 'react-alert';
import { STEP } from '../constants';
import useAccount from '../hooks/useAccount';

const InvestDashboard = () => {

    const alert = useAlert();
    const handleConnectWalletFailure = (e: Error) => {
        alert.show('Please install metamask!');
    }

    const handleConnectWalletSuccess = () => {
        alert.success('Successfully connected with metamask!');
    }

    const onConfirmClaimSuccess = () => {
        alert.success("Claim successful");
    }

    const onConfirmClaimFailure = (e: Error) => {
        alert.show(`Nothing was claimed: ${e.message}`);
    }

    const handleWalletUpdateError = (e: Error) => {
        alert.show(`Unable to update wallet: ${e.message}`);
    }
    
    const handleClaimError = (e: Error) => {
        alert.show(e.message);
    }

    const onWeb3AccountChange = (web3Account: any) => {
        alert.success('Successfully switched account!');
    }

    const { 
        web3Account,
        shillAllotment,
        amountToClaim,
        claimedToken,
        balance,
        progress,
        end,
        currentStep,
        handleConfirmClaim,
        handleCancelClick,
        handleClaimClick,
        handleMetamaskClick
    } = useAccount({ 
            handleConnectWalletSuccess, 
            handleConnectWalletFailure, 
            onConfirmClaimSuccess, 
            onConfirmClaimFailure,
            handleWalletUpdateError,
            handleClaimError,
            onWeb3AccountChange
        });

    return (
        <div className={styles['invest-dashboard-container']}>
            <BalanceInformations
                step={STEP}
                currentStep={currentStep}
                account={web3Account}
                balance={balance.toLocaleString()}
            />
            <div className={styles['invest-dashboard-content-container']}>
                {currentStep === STEP.STEP_1 && (
                    <ConnectWallet
                        onMetamaskClick={handleMetamaskClick}
                        onWalletTrustClick={handleMetamaskClick}
                    />
                )}
                <>
                    {currentStep === STEP.STEP_2 && (
                        <ShillAllotment
                            progress={progress}
                            end={end}
                            shillAllotment={shillAllotment}
                            onClaimClick={handleClaimClick}
                            step={STEP}
                            currentStep={currentStep}
                            claimedToken={claimedToken}
                            amountToClaim={amountToClaim}
                        />
                    )}
                    {currentStep === STEP.STEP_3 && (
                        <ClaimBox
                            amountToClaim={amountToClaim}
                            onConfirmationClick={handleConfirmClaim}
                            onCancelClick={handleCancelClick}
                        />
                    )}
                    {currentStep === STEP.STEP_4 && (
                        <ShillAllotment
                            progress={progress}
                            end={end}
                            shillAllotment={shillAllotment}
                            onClaimClick={handleClaimClick}
                            step={STEP}
                            currentStep={currentStep}
                            claimedToken={claimedToken}
                            amountToClaim={amountToClaim}
                        />
                    )}
                </>
            </div>
        </div>
    );
};

export default InvestDashboard;
