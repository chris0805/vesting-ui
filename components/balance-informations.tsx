import React from "react";
import styles from "./balance-informations.module.scss";

const balanceInformations = ({ step, currentStep, account, balance }) => {
    return (
        <div className={styles["balance-informations-container"]}>
            <section className={styles["balance-informations-title"]}>
                <h1>Investor</h1>
                <h1>Dashboard</h1>
            </section>

            {currentStep !== step.STEP_1 && (
                <section className={styles["balance-informations-content"]}>
                    <div className={styles["balance-informations-item"]}>
                        <h3>Wallet Address</h3>
                        <p>{account || ''}</p>
                    </div>
                    <div className={styles["balance-informations-item"]}>
                        <h3>Balance In Wallet</h3>
                        <p id={styles["balance-amount"]}>
                            <img alt="SHILL-Logo" src="/SHILL_Logo.svg" /> <span>{balance || 0}</span>
                        </p>
                    </div>
                </section>
            )}
        </div>
    );
};

export default balanceInformations;
