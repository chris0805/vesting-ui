import React from "react";
import styles from "./custom-button.module.scss";

const CustomButton = (props) => {
    return (
        <button
            className={styles["custom-button"]}
            style={{
                backgroundColor: `${props.backgroundColor || "#49464c"}`,
                maxWidth: `${props.maxWidth || "125px"}`,
                minHeight: `${props.minHeight || "40px"}`,
                cursor: props.disabled ? "not-allowed" : "pointer",
            }}
            onClick={props.onClick}
            disabled={props.disabled || false}>
            {props.children}
        </button>
    );
};

export default CustomButton;
