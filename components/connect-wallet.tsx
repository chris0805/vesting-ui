import React from "react";
import styles from "./connect-wallet.module.scss";

const ConnectWallet = (props) => {
    return (
        <div className={styles["connect-wallet-container"]}>
            <h3>Connect Wallet</h3>
            <button onClick={props.onMetamaskClick}>
                <img id={styles["button-icon-fox"]} src="/fox-icon.svg" alt="fox-icon" />
                Metamask
            </button>
        </div>
    );
};

export default ConnectWallet;
