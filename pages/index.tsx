import Head from "next/head";
import styles from "../styles/Home.module.scss";
import InvestDashboard from "../components/invest-dashboard";
import AlertTemplate from "react-alert-template-basic";
import { positions, Provider } from "react-alert";

const options = {
    timeout: 3000,
    position: positions.BOTTOM_CENTER
  };

export default function Home() {
    return (
        <Provider template={AlertTemplate} {...options}>
            <div className={styles.container}>
                <Head>
                    <title>Investor Dashboard</title>
                    <meta name="description" content="Generated by create next app" />
                    <link rel="icon" href="/favicon.ico" />
                </Head>
                <InvestDashboard />
            </div>
        </Provider>
    );
}
