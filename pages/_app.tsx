import type { AppProps } from "next/app";
import "../styles/globals.scss";
import Navbar from "../components/Navbar/Navbar";
import AppWrapper from "../context/state";

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <AppWrapper>
            <Navbar>
                <Component {...pageProps} />
            </Navbar>
        </AppWrapper>
    );
}

export default MyApp;
