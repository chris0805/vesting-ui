import React, { useEffect, useRef, useState } from 'react';
import getWeb3 from '../getWeb3';
import { useAppContext } from '../context/state';
import { useRouter } from 'next/dist/client/router';
import FACTORY_ABI from '../abis/factory.json';
import SHILL_ABI from '../abis/shill.json';
import VESTING_ABI from '../abis/vesting.json';
import { STEP } from '../constants';

const SHILL_ADDRESS = '0x1b92Dd385c07b47e0573D0Dac9CCC545B96D7507';
const VESTING_FACTORY_ADDRESS = '0x1FBF3dD4f584c4b9942fcBd3d217eB047623BDA5';

interface UseAccountInterface {
    readonly handleConnectWalletFailure?: Function;
    readonly handleConnectWalletSuccess?: Function;
    readonly onConfirmClaimSuccess?: Function;
    readonly onConfirmClaimFailure?: Function;
    readonly handleWalletUpdateError?: Function;
    readonly handleClaimError?: Function;
    readonly onWeb3AccountChange?: Function;
}

function useAccount({ 
    handleConnectWalletFailure,
    handleConnectWalletSuccess,
    onConfirmClaimSuccess,
    onConfirmClaimFailure,
    handleWalletUpdateError,
    handleClaimError,
    onWeb3AccountChange,
}: UseAccountInterface) {

    const { web3Account, setWeb3Account, currentStep, setCurrentStep } = useAppContext();
    const [vestingContract, setVestingContract] = useState(null);
    const [shillAllotment, setShillAllotment] = useState(0);
    const [amountToClaim, setAmountToClaim] = useState(0);
    const [claimedToken, setClaimedToken] = useState(0);
    const [balance, setBalance] = useState(0);    
    const [progress, setProgress] = useState(0);
    const [end, setEnd] = useState(new Date().toLocaleDateString('en-US'));    
    const [loading, setLoading] = useState(true);
    const refreshTimeoutRef = useRef<NodeJS.Timer>(null);
    
    const router = useRouter();
    const WALLET_PARAMS = "wallet";

    const setWalletUrlParams = (account) => {
        router.query[WALLET_PARAMS] = account;
        router.push(router);
    }    

    const connectWallet = async () => {
        setLoading(true);
        const web3: any = await getWeb3();
        if (web3) {
            const accounts = await web3.eth.getAccounts();
            const account = accounts[0];
            setWalletUrlParams(account);
            setWeb3Account(account);
            await updateWallet(account);
            setCurrentStep(STEP.STEP_2);
            handleConnectWalletSuccess();
        } else {
            handleConnectWalletFailure(new Error('Please install metamask!'));
        }

        setLoading(false);
    }

    const handleAccountChange = async (accounts) => {
        const newWeb3Account = accounts[0];
        if (web3Account != newWeb3Account) {
            onWeb3AccountChange(newWeb3Account);
        }
        setWeb3Account(newWeb3Account);
        setWalletUrlParams(newWeb3Account);
        updateWallet(newWeb3Account);
    }

    const updateWallet = async (web3Account) => {
        setLoading(true);
        try {
            const web3: any = await getWeb3();
            const contract = new web3.eth.Contract(SHILL_ABI, SHILL_ADDRESS);
            const factory = new web3.eth.Contract(FACTORY_ABI, VESTING_FACTORY_ADDRESS);
            console.log('factory ', factory, web3Account);   
            const contractBalance = await contract.methods.balanceOf(web3Account).call();
            setBalance(Math.trunc(contractBalance));

            const vestingAddr = await factory.methods.recipients(web3Account).call();
            const vestingBalanceWei = await contract.methods.balanceOf(vestingAddr).call();
            const vestingBalance = web3.utils.fromWei(vestingBalanceWei, 'ether');

            console.log("vBalance:", vestingBalance, vestingAddr);
            const vesting = new web3.eth.Contract(VESTING_ABI, vestingAddr);
            setVestingContract(vesting);        

            const vestingAmount = await vesting.methods.amount().call();
            setShillAllotment(vestingAmount);

            const cliff = await vesting.methods.cliff().call();
            console.log('cliff', cliff);
            var dateX = new Date();
            // (current timestamp - begin) / (end - begin) * 100
            var dateC = new Date(cliff * 1000);
            console.log("amnt to claim", cliff);

            const begin = await vesting.methods.begin().call();
            console.log('begin', begin);
            var dateB = new Date(begin * 1000);

            const lastUpdate = await vesting.methods.lastUpdate().call();
            const ldate = new Date(lastUpdate * 1000);

            const end = await vesting.methods.end().call();
            console.log("end", end);
            var date = new Date(end * 1000);

            console.log("end", end);
            var date = new Date(end * 1000);

            if (dateX.getTime() < dateC.getTime()) {
                setProgress(0);
                setAmountToClaim(0);
                setClaimedToken(0);

            } else if (dateX.getTime() >= date.getTime()) {
                const clmd = vestingAmount - vestingBalance;
                setProgress(100);
                setAmountToClaim(vestingBalance);
                setClaimedToken(clmd);
                console.log("vbalance ", vestingBalance);

            } else {
                let amntR = (vestingAmount * (dateX.getTime() - ldate.getTime())) / (date.getTime() - dateB.getTime());
                amntR = Math.round(amntR); // web3.utils.fromWei(Math.round(amntR, 'gwei');
                console.log("amount to release ", vestingAmount, vestingBalance, amntR);
                setAmountToClaim(amntR);
                
                const clmd = vestingAmount - vestingBalance;
                setClaimedToken(clmd);
                
                let vbl = clmd + amntR;
                vbl = ( vbl / vestingAmount) * 100;
                vbl = Math.round(vbl);
                console.log("progress   ", vbl);
                setProgress(vbl);
            }                                                                   
            
            console.log("timestamp", dateX.getTime(), dateB.getTime());
            const endDate = date.toUTCString();
            console.log("endDate", endDate);
            setEnd(endDate);

        } catch(err) {
            console.error(err);
        }

        setLoading(false);
    }


    const pollUpdateWalletInfo = async (wallet) => {     
        refreshTimeoutRef.current = setTimeout(async () => {
            if (wallet) {
                console.log(`Refreshing ${new Date()}`);
                await updateWallet(wallet);
                await pollUpdateWalletInfo(wallet);
            }
        }, 10000);                
    }

    const handleClaimClick = async (claimValue) => {
        if (!vestingContract) {
            handleClaimError(new Error('Unable to establish connection to contract'));
            return false;
        }

        if (claimValue && claimValue > 0) {
            setAmountToClaim(claimValue);
            setCurrentStep(STEP.STEP_3);
        } else {
            handleClaimError(new Error('Amount claimable must be valid'));
        }
    }

    const handleConfirmClaim = async (amountToClaimValue, vestingAddr) => {
        const amountParsed = parseInt(amountToClaimValue);
        console.log('amountParsed', amountParsed);
        try {
            await vestingContract.methods.claim().send({ from: web3Account });
            setClaimedToken((totalClaimed) => totalClaimed + amountParsed);
            console.log("Amount claimed: "+amountToClaimValue);
            setCurrentStep(STEP.STEP_4);
            onConfirmClaimSuccess();
        } catch(e){
            console.error("Amount claimed Error: ", e);
            setCurrentStep(STEP.STEP_2);
            if (e.code == 4001){
                onConfirmClaimFailure(new Error('User denied transaction'));
            } else {
                onConfirmClaimFailure(e);
            }
        }        
    }

    const handleMetamaskClick = () => {
        connectWallet();
    }

    const handleCancelClick = () => {
        setCurrentStep(STEP.STEP_2);
    }

    const setupEthereumWallet = async () => {
        if (window.ethereum) {
            window.ethereum.on('accountsChanged', handleAccountChange);
            const params = new URLSearchParams(window.location?.search);
            const wallet = params.get(WALLET_PARAMS);
            if (window.ethereum.isConnected() && wallet) {
                setLoading(true);
                setWeb3Account(wallet);
                setCurrentStep(STEP.STEP_2);
                updateWallet(wallet);
                pollUpdateWalletInfo(wallet);
                setLoading(false);
            }
        }
    }

    const stopEthereumWallet = () => {
        if (window.ethereum) {
            window.ethereum.removeListener('accountsChanged', handleAccountChange);
        }        

        if (refreshTimeoutRef.current) {
            clearTimeout(refreshTimeoutRef.current);
            refreshTimeoutRef.current = null;
        }
    }    

    useEffect(() => {
        setupEthereumWallet();
        return () => {
            stopEthereumWallet();
        }
    }, []);

    return {
        web3Account,
        vestingContract,
        shillAllotment,
        amountToClaim,
        claimedToken,
        balance,
        progress,
        end,
        loading,
        currentStep,
        setCurrentStep,
        connectWallet,
        updateWallet,
        handleConfirmClaim,
        handleCancelClick,
        handleClaimClick,
        handleMetamaskClick
    }
}

export default useAccount;