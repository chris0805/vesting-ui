import { createContext, useContext, useState } from "react";
import { STEP } from "../constants";

interface StateProps {
    web3Account: Object;
    setWeb3Account: Function;
    currentStep: String;
    setCurrentStep: Function;
}

const AppContext = createContext<StateProps>(undefined);

const AppWrapper = ({ children }) => {
    const [currentStep, setCurrentStep] = useState(STEP.STEP_1);
    const [web3Account, setWeb3Account] = useState();
    let sharedState = {
        web3Account,
        setWeb3Account,
        currentStep,
        setCurrentStep
    };

    return (
        <AppContext.Provider value={sharedState} >
            {children}
        </AppContext.Provider>
    )
}

export function useAppContext() {
    return useContext(AppContext);
}

export default AppWrapper;