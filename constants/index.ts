export const API_URL = "https://api.thegraph.com/subgraphs/name/imrowdyninja/seed-vesting";
export const SHILL_TOKEN = "0x1b92Dd385c07b47e0573D0Dac9CCC545B96D7507";

// export const TGE = 1631887520; // begin time

// export const SEED_SALE = {
//   cliff: 7776000, // 90 days
//   end: TGE + 86400 * 380, // 380 days from TGE
// };

// export const PRIVATE_SALE = {
//   cliff: 2592000, // 30 days
//   end: TGE + 86400 * 185, // 185 days from TGE
// };

export const TGE = 1631957400; // begin time

export const SEED_SALE = {
  cliff: 0, // 90 days
  end: TGE + 86400 * 2, // 380 days from TGE
};

export const PRIVATE_SALE = {
  cliff: 0, // 30 days
  end: TGE + 86400 * 2, // 185 days from TGE
};

export const STEP = {
  STEP_1: "CONNECT_WALLET",
  STEP_2: "SHILL_ALLOTMENT",
  STEP_3: "CLAIM_SEED",
  STEP_4: "SEED_CLAIMED",
};