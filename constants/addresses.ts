import { SupportedChainId } from "./chains";

type AddressMap = { [chainId: number]: string };

export const VESTING_FACTORY_ADDRESS: AddressMap = {
  [SupportedChainId.GOERLI]: "0xa4BaC9781Ca519EC7F46498D96305714de6e92CA",
  [SupportedChainId.BSC]: "",
  [SupportedChainId.BSC_TESTNET]: "0x22c9ece2ce2e3e924478305212dd64a527dfdf02",
};
