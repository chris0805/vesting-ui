export enum SupportedChainId {
  GOERLI = 5,
  BSC = 56,
  BSC_TESTNET = 97,
}

export const NETWORK_LABELS: { [chainId in SupportedChainId | number]: string } = {
  [SupportedChainId.GOERLI]: "Goerli",
  [SupportedChainId.BSC]: "BSC Mainnet",
  [SupportedChainId.BSC_TESTNET]: "BSC Testnet",
};
